from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from problems.models import MathProblems, Solution, Topic
from problems.forms import MathQuestionAskForm, MathQuestionAnswerForm, TopicForm

def home_page(request):
    return render(request, "problems/home.html")

def all_topics(request):
    all_topics = Topic.objects.all()
    images = ['https://www.psychologicalscience.org/redesign/wp-content/uploads/2014/08/PAFF_082917_mathequations-609x419.jpg',
                   'https://media.istockphoto.com/id/1219382595/vector/math-equations-written-on-a-blackboard.jpg?s=612x612&w=0&k=20&c=ShVWsMm2SNCNcIjuWGtpft0kYh5iokCzu0aHPC2fV4A=',
                   'https://i.pinimg.com/originals/1d/13/ea/1d13ead1c85dc568429e9672b48875e2.jpg',
                   ]
    context={
        "all_topics": all_topics,
        "images": images,
    }
    return render(request, "problems/topic_list.html", context)


def all_problems(request, topic_id=None):
    topics = Topic.objects.all().prefetch_related('problems')
    if topic_id:
        problems = MathProblems.objects.filter(topic_id=topic_id)
    else:
        problems = MathProblems.objects.all()
    context = {
        "topics": topics,
        "problems": problems,
    }
    return render(request, "problems/question_list.html", context)


@login_required
def my_problems(request):
    my_problems = MathProblems.objects.filter(question_creater=request.user)
    context={
        "my_problems": my_problems,
    }
    return render(request, "problems/my_question.html", context)

@login_required
def add_a_topic(request):
    if request.method == "POST":
        form = TopicForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("all_topics")
    else:
        form = TopicForm
        context = {
            "form": form,
        }
        return render(request, "problems/add_topic.html", context)

@login_required
def ask_a_problem(request):
    topics = Topic.objects.all()
    users = User.objects.all()

    if request.method == "POST":
        form = MathQuestionAskForm(request.POST)
        if form.is_valid():
            problem = form.save(False)
            problem.question_creater = request.user
            form.save()
            return redirect("my_problems")
    else:
        form = MathQuestionAskForm
        context = {
            "form": form,
            "topics": topics,
            "users": users,
        }
        return render(request, "problems/ask_question.html", context)

@login_required
def individual_math_problems(request, id):
    problems = get_object_or_404(MathProblems, id=id)
    context={
        "math_problems": problems,
    }
    return render(request, "problems/question.html", context)

@login_required
def answer_a_problem(request, id):
    question = get_object_or_404(MathProblems, id=id)
    users = User.objects.all()

    if request.method == "POST":
        form = MathQuestionAnswerForm(request.POST)
        if form.is_valid():
            solution = form.save(False)
            solution.solution_creater = request.user
            solution.math_problem = question
            form.save()
            return redirect("individual_problem", id=id)
    else:
        form = MathQuestionAnswerForm
    context = {
        "form": form,
        "question": question,
        "users": users,
    }
    return render(request, "problems/answer_question.html", context)

@login_required
def like_solution(request, id):
    solution = get_object_or_404(Solution, id=id)
    solution.likes += 1
    solution.save()
    return redirect("individual_problem", id=solution.question.id)

@login_required
def dislike_solution(request, id):
    solution = get_object_or_404(Solution, id=id)
    solution.dislikes += 1
    solution.save()
    return redirect("individual_problem", id=solution.question.id)
