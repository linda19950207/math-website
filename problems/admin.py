from django.contrib import admin
from problems.models import MathProblems, Solution, Topic

# Register your models here.
@admin.register(MathProblems)
class MathProblemsAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "topic",
        "question",
    )

@admin.register(Solution)
class SolutionAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "solution",
    )

@admin.register(Topic)
class TopicAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "name",
    )
