# Generated by Django 4.2 on 2023-04-22 19:28

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ("problems", "0001_initial"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="mathproblems",
            name="solution",
        ),
        migrations.AddField(
            model_name="solution",
            name="question",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name="solutions",
                to="problems.mathproblems",
            ),
        ),
    ]
