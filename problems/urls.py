from django.urls import path
from . import views

urlpatterns = [
    path("", views.home_page, name="home"),
    path("problems/<int:topic_id>", views.all_problems, name="problems_by_topic"),
    path("myproblems", views.my_problems, name="my_problems"),
    path("topics", views.all_topics, name="all_topics"),
    path("topics/add", views.add_a_topic, name="add_topics"),
    path("<int:id>", views.individual_math_problems, name="individual_problem"),
    path("ask", views.ask_a_problem, name="ask_a_problem"),
    path("<int:id>/solution", views.answer_a_problem, name="answer_a_problem"),
    path("<int:id>/solution/likes", views.like_solution, name = "like_solution"),
    path("<int:id>/solution/dislikes", views.dislike_solution, name = "dislike_solution"),
]
