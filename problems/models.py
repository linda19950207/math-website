from django.db import models
from django.conf import settings

class Topic(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField(null=True)

    def __str__(self):
        return self.name

class MathProblems(models.Model):
    question = models.TextField(null=True)
    questions_created_on = models.TimeField(auto_now_add=True)
    question_creater = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="questions",
        on_delete=models.CASCADE,
    )
    topic = models.ForeignKey(
        Topic,
        related_name="problems",
        on_delete=models.CASCADE,
    )

    class Meta:
        ordering = ["topic"]

    def __str__(self):
        return self.question

class Solution(models.Model):
    solution = models.TextField(null=True)
    solutions_created_on = models.TimeField(auto_now=True)
    solution_creater = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="solutions",
        on_delete=models.CASCADE,
    )
    question = models.ForeignKey(
        MathProblems,
        related_name="solutions",
        on_delete=models.CASCADE,
        null=True,
    )
    likes = models.PositiveIntegerField(default = 0, null = True, blank = True)
    dislikes = models.PositiveIntegerField(default = 0, null = True, blank = True)

    class Meta:
        ordering = ["solutions_created_on"]
