from problems.models import MathProblems, Topic, Solution
from django.forms import ModelForm

class MathQuestionAskForm(ModelForm):
    class Meta:
        model = MathProblems
        fields=["topic", "question", "question_creater"]


class MathQuestionAnswerForm(ModelForm):
    class Meta:
        model = Solution
        fields=["question", "solution", "solution_creater"]


class TopicForm(ModelForm):
    class Meta:
        model = Topic
        fields=["name", "description"]
